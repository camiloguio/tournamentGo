$(document).ready(function() {
    $('.js-example-basic-single').select2();
});

jQuery( document ).ready(function() {
    $("#logincampos").fadeIn(1000);
});

jQuery( document ).ready(function() {
    $("#login").fadeIn(1000);
});
/* Abrir Alerta */
function alerta(){

    event.preventDefault();
    jQuery("div.alert").delay(300).fadeIn(300);
    jQuery("div.alert p").delay(300).fadeIn(300);
    jQuery("div.background-alert-form").delay(300).fadeIn(300);

    return false;

}

/* Cerrar Alerta */
jQuery("div.alert div#cerrar img#cerrarimg").click(function(){
    jQuery("div#alert").fadeOut(300);
    jQuery("div.background-alert-form").fadeOut(300);
})
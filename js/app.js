(function () {

var app = angular.module('dashboardusr', []);

app.directive('headTgo', function(){

    return {
        restrict: 'E',
        templateUrl: 'partials/head.html'
    };
});

app.directive('headerTgo', function(){

    return {
        restrict: 'E',
        templateUrl: 'partials/header.html'
    };
});

app.directive('menuTgo', function(){

    return {
        restrict: 'E',
        templateUrl: 'partials/menu.html'
    };
});

app.directive('formteamTgo', function(){

    return {
        restrict: 'E',
        templateUrl: 'partials/formequipo.html'
    };
});

app.directive('elegirPlayer', function(){

    return {
        restrict: 'E',
        templateUrl: 'partials/elegirplayer.html'
    };
});

})();